import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.StringBuilder;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import java.nio.charset.StandardCharsets;
import java.io.IOException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.FileSystems;
import org.apache.commons.io.FileUtils;

import javax.net.ssl.HttpsURLConnection;
import java.text.SimpleDateFormat;

public class Parser {

	private final static String airTableAPIKey = "key92eFMebVCNiwJn";

	public static void main (String[]args) throws Exception {
		try {
			parseFiles();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public static void parseFiles() throws IOException, ParseException, InterruptedException {

		Path output = FileSystems.getDefault().getPath("output.txt");
		Path directory = FileSystems.getDefault().getPath("s3files");

		Stream<Path> filesToProcess = Files.list(directory);

		StringBuilder sb = new StringBuilder("");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		// Read in previous output.csv to find most recent date
		File fileCSV = new File(output.toString());
		String content = FileUtils.readFileToString(fileCSV, "utf-8");
		JSONArray inputJson = new JSONArray(content);

		Date mR = new Date(Long.MIN_VALUE);
		for(int i = 0 ; i < inputJson.length(); i++) {
			JSONObject jsonObject = inputJson.getJSONObject(i);
			String date = jsonObject.getString("date");
			Date currentDate = sdf.parse(date);
			if ( currentDate.after(mR)) {
				mR = currentDate;
			}
		}

		final Date mostRecent = mR;
		// Date mostRecent = sdf.parse("5/21/2019 00:57:00");  // use to run from specific date

		// Iterate all files to create json string from each file
		sb.append("[");

		sb.append("{\"firstName\":\"\",\"email\":\"\",\"country\":\"\",\"type\":\"\",\"priority\":\"\",\"legalBusinessName\":\"\",\"state\":\"\",\"acceptTerms\":\"\",\"receiveEmails\":\"\",\"companyType\":\"\",\"name\":\"\",\"phoneNumber\":\"\",\"premiumService\":\"\",\"usBank\":\"\",\"orderTypes__001\":\"\",\"orderTypes__002\":\"\",\"orderTypes__003\":\"\",\"orderTypes__004\":\"\",\"orderTypes__005\":\"\",\"investmentStyle\":\"\",\"accreditedInvestor\":\"\",\"monthlyVolume\":\"\",\"averageBalance\":\"\",\"lastName\":\"\",\"date\":\"01/01/1990 00:00:00\"},");

		// Airtable Change List
		List<JSONObject> airtableArray = new ArrayList<>();

		filesToProcess.forEach(path -> {
			// Get all lines of that file
			try {
				Stream<String> lines = Files.lines(path);
				lines.forEach(line -> {
					// Parse new file and write to json output
					File file = new File(path.toString());
					Date fileDate = new Date(file.lastModified());
					if (fileDate.after(mostRecent)) {
						String lineToWrite = line.substring(0, line.length()-1) + " ,\"date\": \"" + sdf.format(file.lastModified()) + "\"}";

						// Create Airtable payload
						JSONObject jsonObject = new JSONObject(lineToWrite);

						// Add status and Client Name field
						jsonObject.put("status",  new JSONArray().put("new"));
						jsonObject.put("Client Name", jsonObject.get("name"));
						jsonObject.put("time signed up", jsonObject.get("date"));
						jsonObject.remove("date");

						if (jsonObject.has("state") && jsonObject.get("state").equals("")) {
							jsonObject.remove("state");
						}

						// Convert boolean -> text fields
						convertBooleanField(jsonObject, "acceptTerms");
						convertBooleanField(jsonObject, "priority");
						convertBooleanField(jsonObject, "premium");
						convertBooleanField(jsonObject, "premiumService");
						convertBooleanField(jsonObject, "accreditedInvestor");
						convertBooleanField(jsonObject, "usBank");
						convertBooleanField(jsonObject, "receiveEmails");

						// Convert multiple choice fields
						convertToMultipleChoice(jsonObject, "type");
						convertToMultipleChoice(jsonObject, "country");
						convertToMultipleChoice(jsonObject, "state");
						convertToMultipleChoice(jsonObject, "priority");
						convertToMultipleChoice(jsonObject, "monthlyVolume");
						convertToMultipleChoice(jsonObject, "premiumService");
						convertToMultipleChoice(jsonObject, "accreditedInvestor");
						convertToMultipleChoice(jsonObject, "averageBalance");
						convertToMultipleChoice(jsonObject, "acceptTerms");
						convertToMultipleChoice(jsonObject, "investmentStyle");

						airtableArray.add(jsonObject);

						// Append to sb
						sb.append(lineToWrite).append(",").append("\n");

						// Write every line to the output file to track most recent change
						try {
							Files.write(output, lineToWrite.getBytes(StandardCharsets.UTF_8));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		// Close and clean
		if(sb.lastIndexOf(",") != -1) {
			sb.deleteCharAt(sb.lastIndexOf(","));
		}
		sb.append("]");

		// Sort array list by date
		Collections.sort(airtableArray, new Comparator<JSONObject>() {
			public int compare(JSONObject o1, JSONObject o2) {
				try {
					Date d1 = sdf.parse(o1.getString("time signed up"));
					Date d2 = sdf.parse(o2.getString("time signed up"));
					return d1.compareTo(d2);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return 0;
			}
		});

		try {
			// Write out json
			PrintWriter out = new PrintWriter(output.toString());
			out.println(sb.toString());

			// Close / flush files
			out.flush();
		}catch (IOException e) {
			e.printStackTrace();
		}

		// Create list of updates
		JSONArray requestList = new JSONArray();
		for ( JSONObject jsonObject : airtableArray) {
			JSONObject postRequest = new JSONObject();
			postRequest.put("fields", jsonObject);
			postRequest.put("typecast", false);
			requestList.put(postRequest);
		}

		for (int i = 0; i < requestList.length(); i++ ) {
			JSONObject post = requestList.getJSONObject(i);

			try {
				sendPost(post);
			} catch (Exception e) {
				System.out.println("Error updating: " + post.toString());
				e.printStackTrace();
			}

			// Stay under rate limit
			TimeUnit.MILLISECONDS.sleep(300);
		}

		System.out.println("Fin~");
	}

	private static String sendPost(JSONObject post) throws IOException {

		// Post new airtable entries
		String url = "https://api.airtable.com/v0/app0zQmOdVCdgkqaR/Dev%20Test";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		con.setRequestMethod("POST");
		con.setRequestProperty("Accept", "application/json");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Authorization","Bearer " + airTableAPIKey);
		con.setDoInput(true);
		con.setDoOutput(true);

		sendData(con, post.toString());
		return read(con.getInputStream());
	}

	private static void convertToMultipleChoice(JSONObject jsonObject, String fieldName) {
		if(jsonObject.has(fieldName) && jsonObject.get(fieldName).equals("")){
			jsonObject.remove(fieldName);
			return;
		}
		// Fix Order Types
		if(jsonObject.has(fieldName)) {
			String entry = (String) jsonObject.get(fieldName);
			jsonObject.put(fieldName, new JSONArray().put(entry.replaceAll("_", " ")));
		}
	}

	private static void convertBooleanField(JSONObject jsonObject, String fieldName) {
		if(jsonObject.has(fieldName)){
			Boolean accept = (Boolean) jsonObject.get(fieldName);
			if (accept) {
				jsonObject.put(fieldName, "TRUE");
			} else {
				jsonObject.put(fieldName, "FALSE");
			}
		}
	}

	private static void sendData(HttpURLConnection con, String data) throws IOException {
		DataOutputStream wr = null;
		try {
			wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(data);
			wr.flush();
			wr.close();
		} catch(IOException exception) {
			System.out.println("throwing?");
			exception.printStackTrace();
			throw exception;
		} finally {
			closeQuietly(wr);
		}
	}

	private static String read(InputStream is) throws IOException {
		BufferedReader in = null;
		String inputLine;
		StringBuilder body;
		try {
			in = new BufferedReader(new InputStreamReader(is));

			body = new StringBuilder();

			while ((inputLine = in.readLine()) != null) {
				body.append(inputLine);
			}
			in.close();

			return body.toString();
		} catch(IOException ioe) {
			System.out.println("Throwiung?");
			ioe.printStackTrace();
			throw ioe;
		} finally {
			closeQuietly(in);
		}
	}

	private static void closeQuietly(Closeable closeable) {
		try {
			if( closeable != null ) {
				closeable.close();
			}
		} catch(IOException ex) {
			ex.printStackTrace();
		}
	}
}
